$(document).ready(function () {

    var getInvitedUsers = function (submitUrl) {
        $.ajax({
            url: submitUrl,
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            },
            success: function (data) { displayInvitations(data); },
            error: function (xhr) {
                console.error(xhr);
            }
        });

    }

    var getCurrentUser = function (submitUrl) {
        $.ajax({
            url: submitUrl,
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            },
            success: function (data) {
                currentUserEmail = data.userEmail;
                console.log(currentUserEmail);
            },
            error: function (xhr) {
                console.error(xhr);
            }
        });

    }

    var doSearchRequest = function (submitUrl) {
        $.ajax({
            url: submitUrl,
            method: 'GET',
            headers: {
                'Accept': 'application/json'
            },
            success: function (data) {
                searchResults = data;
                displayUsersFound(data);
            },
            error: function (xhr) {
                console.error(xhr);
            }
        });

    }

    var displayUsersFound = function (data) {
        $('#results').html('');
        data._embedded.usersearch.forEach(user => {
            var pos = invitedList.indexOf(user.userEmail);
            var row = '';
            if (currentUserEmail != user.userEmail) {
                if (pos < 0) {
                    row = '<tr><td><input type="button" value="Invite colleague"  class="inviteButton" id="' + user.userEmail + '"/></td><td>' + user.userName + '</td><td>' + user.userEmail + '</td></tr>'; //data-id                                    
                }
                else {
                    row = '<tr><td>Colleague invited</td><td>' + user.userName + '</td><td>' + user.userEmail + '</td></tr>'; //data-id                                    
                }
                $('#results').append(row);

            }

        });

    };

    var displayInvitations = function (data) {
        $('#invitations').html('');
        $('#invited_count').html('');

        let i = 0;

        data.eventInvites.forEach(event_invite => {
            i++;
            invitedList.push(event_invite.userEmail);
            if ('true' == event_invite.inviteDeclined) {
                var row = '<tr bgcolor="#c1b8ba"><td>' + event_invite.userName + '</td><td>' + event_invite.userEmail + '</td><td>Invitation declined</td></tr>';
            }
            else {
                var row = '<tr><td>' + event_invite.userName + '</td><td>' + event_invite.userEmail + '</td></tr>';

            }
            $('#invitations').append(row);

        });
        $('#invited_count').append("<p><b>You have invited " + i + " colleague(s)</b>:</p>");

    };

    let searchParams = new URLSearchParams(window.location.search);
    let event = searchParams.get('event');
    var invitedUsersUrl = './getInvitedUsers?event=' + event;
    var currentUserUrl = './getCurrentUser';

    let invitedList = [];
    let currentUserEmail = '';
    let searchResults = '';
    getInvitedUsers(invitedUsersUrl);
    getCurrentUser(currentUserUrl);

    $('.searchresults').on("click", ".inviteButton", function (e) {
        e.preventDefault();
        let email = $(this).attr('id'); //$(this).data("id")
        $.ajax({
            url: './saveInvite',
            method: 'POST',
            data: JSON.stringify({ invitedUserEmail: email, eventId: event }),
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            success: function (data) {
                console.log(data);
                getInvitedUsers(invitedUsersUrl);
                invitedList.push(data.userEmail);
                displayUsersFound(searchResults);
            }
        });


    });

    $('#submitButton').click(function (e) {
        e.preventDefault();
        console.log('Button clicked');

        var username = $('#username').val();
        var department = $('#department').val();
        var submitUrl = '';
        if (department == '0') {
            submitUrl = './usersearch/search/findByUserNameContaining?name=' + username;
        }
        else {
            submitUrl = './usersearch/search/findByDepartmentAndName?department=' + department + '&name=' + username;
        }
        doSearchRequest(submitUrl);

    });

});

