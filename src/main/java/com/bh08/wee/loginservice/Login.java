package com.bh08.wee.loginservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bh08.wee.service.SessionService;
import com.bh08.wee.viewmodel.data.user.LoginFormData;

@Service
public class Login {

	@Autowired
	private SessionService sessionService;

	public String handleInvalidLogin(String pageRedirecting, RedirectAttributes redirectAttributes) {
		LoginFormData loginFormData = new LoginFormData();
		redirectAttributes.addFlashAttribute("loginFormData", loginFormData);
		sessionService.setPageRedirectingToLogin(pageRedirecting);
		return "redirect:login";
	}

}
