package com.bh08.wee.builders;

import java.time.LocalDate;

import com.bh08.wee.model.Department;
import com.bh08.wee.model.Gender;
import com.bh08.wee.model.User;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

public class UserBuilder {
	private String userEmail;
	private String userPassword;
	private String userName;
	private String userBirthPlace;
	private LocalDate userBirthDate;
	private Gender userGender;
	private String userCity;
	private String userProfilePic;
	private Department userDepartment;
	private String userSchools;
	private String userWorkplaces;

	private static UserBuilder userBuilder;

	public UserBuilder() {
	}

	public static UserBuilder getUserBuilder() {
		if (null == userBuilder) {
			userBuilder = new UserBuilder();
		}
		return userBuilder;

	}

	public User build() {
		return new User(userEmail, userPassword, userName, userBirthPlace, userBirthDate, userGender, userCity,
				userProfilePic, userDepartment, userSchools, userWorkplaces);
	}

	public String getUserEmail() {
		return userEmail;
	}

	public UserBuilder setUserEmail(String userEmail) {
		this.userEmail = userEmail;
		return this;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public UserBuilder setUserPassword(String userPassword) {
		this.userPassword = userPassword;
		return this;
	}

	public String getUserName() {
		return userName;
	}

	public UserBuilder setUserName(String userName) {
		this.userName = userName;
		return this;
	}

	public String getUserBirthPlace() {
		return userBirthPlace;
	}

	public UserBuilder setUserBirthPlace(String userBirthPlace) {
		this.userBirthPlace = userBirthPlace;
		return this;
	}

	public LocalDate getUserBirthDate() {
		return userBirthDate;
	}

	public UserBuilder setUserBirthDate(LocalDate userBirthDate) {
		this.userBirthDate = userBirthDate;
		return this;
	}

	public Gender getUserGender() {
		return userGender;
	}

	public UserBuilder setUserGender(Gender userGender) {
		this.userGender = userGender;
		return this;
	}

	public String getUserPlace() {
		return userCity;
	}

	public UserBuilder setUserCity(String userCity) {
		this.userCity = userCity;
		return this;
	}

	public String getUserProfilePic() {
		return userProfilePic;
	}

	public UserBuilder setUserProfilePic(String userProfilePic) {
		this.userProfilePic = userProfilePic;
		return this;
	}

	public Department getUserDepartment() {
		return userDepartment;
	}

	public UserBuilder setUserDepartment(Department userDepartment) {
		this.userDepartment = userDepartment;
		return this;
	}

	public String getUserSchools() {
		return userSchools;
	}

	public UserBuilder setUserSchools(String userSchools) {
		this.userSchools = userSchools;
		return this;
	}

	public String getUserWorkplaces() {
		return userWorkplaces;
	}

	public UserBuilder setUserWorkplaces(String userWorkplaces) {
		this.userWorkplaces = userWorkplaces;
		return this;
	}

}