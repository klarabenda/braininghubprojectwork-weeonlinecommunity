package com.bh08.wee.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.wee.daos.ArticleDAO;
import com.bh08.wee.model.Event;
import com.bh08.wee.model.News;

@Service
public class ArticleService {

	@Autowired
	ArticleDAO articleDao;

	public News saveArticle(News article) {
		return articleDao.saveAndFlush(article);
	}

	public Optional<News> getNewsById(Long articleId) {
		return articleDao.findById(articleId);
	}

	public List<News> getAllArticles() {
		return articleDao.findAll();
	}

	public List<News> getCurrentArticles() {
		return articleDao.findByNewsAvailableUntilAfterOrderByNewsAvailableUntilAsc(LocalDate.now());
	}
}
