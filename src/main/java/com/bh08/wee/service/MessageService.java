package com.bh08.wee.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.wee.daos.MessageDAO;
import com.bh08.wee.daos.UserDAO;
import com.bh08.wee.model.Message;
import com.bh08.wee.model.User;

@Service
public class MessageService {
	
	@Autowired
	private MessageDAO messageDao;
	
	@Autowired
	private UserDAO userDao;
	
	public List<Message> getAllMessages() {
		return messageDao.findAll();
	}
	
	public List<Message> getAllByMessageTo(User messageTo) {
		return messageDao.findByMessageToOrderByMessageDateDesc(messageTo);
	}
	
	public List<Message> getAllByMessageFrom(User messageFrom) {
		return messageDao.findByMessageFrom(messageFrom);
	}
	
	public void sendMessage(Message message) {
		if (null!=message) {
			messageDao.saveAndFlush(message);
		}
	}
	
}
