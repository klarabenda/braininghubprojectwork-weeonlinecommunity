package com.bh08.wee.service;

import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Service
@ToString
@SessionScope
@AllArgsConstructor
@NoArgsConstructor
public class SessionService {
	
	private Long currentUserId;

	private String pageRedirectingToLogin = "status";

}
