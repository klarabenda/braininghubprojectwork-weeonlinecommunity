package com.bh08.wee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.wee.daos.DepartmentDAO;
import com.bh08.wee.model.Department;

@Service
public class DepartmentService {

	@Autowired
	private DepartmentDAO departmentDao;

	public Optional<Department> getDepartmentById(Long departmentId) {
		return departmentDao.findById(departmentId);
	}
	
	public Optional<Department> getDepartmentByName(String departmentName) {
		return departmentDao.findByDepartmentName(departmentName);
	}

	public Department saveDepartment(String departmentName) {
		Department department = new Department();
		department.setDepartmentName(departmentName);
		return departmentDao.saveAndFlush(department);
	}

	public List<Department> getAllDepartments() {
		return departmentDao.findAll();
	}
}
