package com.bh08.wee.emailservice;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.stereotype.Service;

import com.bh08.wee.model.Event;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;

@Service
public class VEventService {
	
	public String createVEvent(Event event){
		ICalendar ical = new ICalendar();
        VEvent vEvent = new VEvent();
        Long uid = event.getEventId()*1111;
        vEvent.setUid(uid.toString());
        vEvent.setDescription(event.getEventDescription());
        vEvent.setSummary(event.getEventTitle());
        vEvent.setOrganizer(event.getEventCreator().getUserEmail());
        vEvent.setLocation(event.getEventLocation());
        
        Date start = localDateTimeToDate(event.getEventFrom());
        Date end = localDateTimeToDate(event.getEventUntil());

        vEvent.setDateStart(start);
        vEvent.setDateEnd(end);
        
		ical.addEvent(vEvent);
		
		String vEventOutput = Biweekly.write(ical).go();
		System.out.println(vEventOutput);
		return vEventOutput;
		
	}
	  
	public Date localDateTimeToDate(LocalDateTime ldt) {
		ZoneId zoneId = ZoneId.of("Europe/Paris");
		ZonedDateTime zdt = ldt.atZone(zoneId);
		Calendar c = GregorianCalendar.from(zdt);
	    return c.getTime();
	  }
}
