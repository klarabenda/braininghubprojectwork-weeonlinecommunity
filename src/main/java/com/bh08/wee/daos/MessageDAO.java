package com.bh08.wee.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bh08.wee.model.Department;
import com.bh08.wee.model.Message;
import com.bh08.wee.model.User;

@Repository
public interface MessageDAO  extends JpaRepository<Message, Long> {
	
	public List<Message> findAll();
	
	public List<Message> findByMessageToOrderByMessageDateDesc(User messageTo);
	
	public List<Message> findByMessageFrom(User messageFrom);
	
	
}

