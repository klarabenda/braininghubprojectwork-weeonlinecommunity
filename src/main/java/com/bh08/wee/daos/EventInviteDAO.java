package com.bh08.wee.daos;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bh08.wee.model.Event;
import com.bh08.wee.model.EventInvite;
import com.bh08.wee.model.User;

@Repository
public interface EventInviteDAO extends JpaRepository<EventInvite, Long> {

	public List<EventInvite> findAll();

	public List<EventInvite> findByEvent(Event event);

	public List<EventInvite> findByEventAndInviteValid(Event event, boolean inviteValid);

	public Integer countByInvitedAndEvent(User user, Event event);

	public List<EventInvite> findByEventIn(List<Event> events);

	public List<EventInvite> findByInvitedAndEventIn(User user, List<Event> events);

	public List<EventInvite> findByInvitedOrderByInviteCreatedAsc(User user);

	List<EventInvite> findByInvitedAndInviteCreatedAfterOrderByInviteCreatedAsc(User user, Date date);

	public Integer countByEvent(Event event);

}
