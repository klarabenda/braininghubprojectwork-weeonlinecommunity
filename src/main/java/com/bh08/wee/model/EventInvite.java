package com.bh08.wee.model;

import java.io.Serializable;
import java.util.Date;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class EventInvite implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long inviteId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn
	private Event event;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn
	private User invited;
	
	@CreationTimestamp
	private Date inviteCreated;

	private boolean inviteValid;

}
