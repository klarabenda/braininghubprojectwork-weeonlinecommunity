package com.bh08.wee.model;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class News implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long newsId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn
	private User newsCreator;

	@Column(nullable = false)
	private Date newsPublished;

	@Column(nullable = false)
	private LocalDate newsAvailableUntil;

	@Column(nullable = false)
	private String newsTitle;

	private String newsText;

}
