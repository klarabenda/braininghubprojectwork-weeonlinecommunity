package com.bh08.wee.controllers;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.mail.MessagingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bh08.wee.exceptions.NoUserException;
import com.bh08.wee.loginservice.Login;
import com.bh08.wee.model.Event;
import com.bh08.wee.model.News;
import com.bh08.wee.model.User;
import com.bh08.wee.service.ArticleService;
import com.bh08.wee.service.SessionService;
import com.bh08.wee.service.UserService;
import com.bh08.wee.viewmodel.data.PageData;
import com.bh08.wee.viewmodel.data.article.NewArticleFormData;
import com.bh08.wee.viewmodel.data.event.NewEventFormData;

@Controller
public class ArticleController {

	@Autowired
	private ArticleService articleService;
	@Autowired
	private UserService userService;
	@Autowired
	private SessionService sessionService;
	@Autowired
	private Login loginControllerService;

	@RequestMapping(value = "main", method = RequestMethod.GET)
	public String displayArticles(Model model, RedirectAttributes redirectAttributes) {
		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			return loginControllerService.handleInvalidLogin("new_event", redirectAttributes);
		} else {
			model.addAttribute("loggedInUserId", sessionService.getCurrentUserId());
			model.addAttribute("Articles", articleService.getCurrentArticles());
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: mainPageMenu");
			pageData.setPageLocation("fragments/mainPage :: mainPage");
			pageData.setMainClass("article");
			model.addAttribute("pageData", pageData);
			return "main.html";
		}
	}
	
	@RequestMapping(value = "new_article", method = RequestMethod.GET)
	public String displayArticleForm(Model model, RedirectAttributes redirectAttributes) {
		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			return loginControllerService.handleInvalidLogin("new_article", redirectAttributes);
		} else {
			model.addAttribute("loggedInUserId", userId);
			NewArticleFormData articleFormData = new NewArticleFormData();
			model.addAttribute("articleFormData", articleFormData);
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: mainPageMenu");
			pageData.setPageLocation("fragments/articleForm :: articleForm");
			pageData.setMainClass("article");
			model.addAttribute("pageData", pageData);
			return "main.html";
		}
	}

	@RequestMapping(value = "new_article", method = RequestMethod.POST)
	public String saveEventData(@ModelAttribute("articleFormData") @Valid NewArticleFormData articleFormData,
			BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {

		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			return loginControllerService.handleInvalidLogin("new_article", redirectAttributes);
		} else {
			if (!bindingResult.hasErrors()) {
				System.out.println("OK");
				Long articleId = 0L;
				User user;
				try {
					user = userService.getUserById(userId);
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
					News article = new News();
					article.setNewsCreator(user);
					article.setNewsPublished(new java.sql.Date(System.currentTimeMillis()));
					article.setNewsAvailableUntil(LocalDate.parse(articleFormData.getNewsAvailableUntil(), formatter));
					article.setNewsTitle(articleFormData.getNewsTitle());
					article.setNewsText(articleFormData.getNewsText());
					articleService.saveArticle(article);
					articleId = article.getNewsId();
				} catch (NoUserException e) {
					e.printStackTrace();
					// TODO
					return "redirect:error";
				}
				return "redirect:/main";
			} else {
				System.out.println("Error");
			}
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: mainPageMenu");
			pageData.setPageLocation("fragments/articleForm :: articleForm");
			pageData.setMainClass("article");
			model.addAttribute("pageData", pageData);
			return "main.html";
		}

	}

	
}
