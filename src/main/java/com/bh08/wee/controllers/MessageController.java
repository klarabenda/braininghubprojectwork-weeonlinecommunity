package com.bh08.wee.controllers;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bh08.wee.exceptions.NoUserException;
import com.bh08.wee.model.Message;
import com.bh08.wee.model.User;
import com.bh08.wee.service.MessageService;
import com.bh08.wee.service.SessionService;
import com.bh08.wee.service.UserService;
import com.bh08.wee.viewmodel.data.PageData;
import com.bh08.wee.viewmodel.data.message.MessageFormData;

@Controller
public class MessageController {

	@Autowired
	private UserController userController;

	@Autowired
	private MessageService messageService;

	@Autowired
	private UserService userService;

	@Autowired
	private SessionService sessionService;

	@InitBinder
	public void allowEmptyDateBinding(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@RequestMapping(value = "/incMessage", method = RequestMethod.GET)
	public String incMessage(Model model) {
		MessageFormData messageFormData = new MessageFormData();
		User currentUser = userController.getCurrentUser();
		
		List<Message> messageList = new ArrayList<>();
		
		if(null!=messageService.getAllByMessageTo(currentUser)) {
			messageList=messageService.getAllByMessageTo(currentUser);
		}
		
		System.out.println("FELHASZNALO: "+currentUser);
		System.out.println("UZENETEK: "+messageList);
		
		model.addAttribute("messages", messageList);
		
		PageData pageData = new PageData();
		pageData.setMenuLocation("fragments/menus :: messageMenu");
		pageData.setPageLocation("fragments/incMessage :: incMessagePage");
		pageData.setMainClass("message");
		model.addAttribute("loggedInUserId", sessionService.getCurrentUserId());
		model.addAttribute("pageData", pageData);
		model.addAttribute("messageFormData", messageFormData);
		return "main.html";
	}
		
	
	@RequestMapping(value = "/newMessage", method = RequestMethod.GET)
	public String newMessage(Model model) {
		MessageFormData messageFormData = new MessageFormData();
		User currentUser = userController.getCurrentUser();
		List<User> users =  userService.getAllUsers();
		PageData pageData = new PageData();
		pageData.setMenuLocation("fragments/menus :: messageMenu");
		pageData.setPageLocation("fragments/newMessage :: newMessagePage");
		pageData.setMainClass("message");
		model.addAttribute("loggedInUserId", sessionService.getCurrentUserId());
		model.addAttribute("pageData", pageData);
		model.addAttribute("users", users);
		model.addAttribute("messageFormData", messageFormData);
		return "main.html";
	}

	
	@RequestMapping(value = "/messageSend", method = RequestMethod.POST)
	public String sendMessage(@ModelAttribute("messageFormData") @Valid MessageFormData messageFormData,
			BindingResult bindingResult, Model model) {

		User currentUser = userController.getCurrentUser();
		Message currentMessage = new Message();

		currentMessage.setMessageFrom(currentUser);
		
		System.out.println("CIMZETT "+messageFormData.getMessageToName());
		
		if (null != messageFormData.getMessageToName()) {
			try {
//				currentMessage.setMessageTo(userService.getUserByUserEmail(messageFormData.getMessageToEmailAddress()));
				currentMessage.setMessageTo(userService.getUserByUserName(messageFormData.getMessageToName()));
			} catch (NoUserException e) {
				bindingResult.rejectValue("messageToEmailAddress", "", e.getMessage());
				e.printStackTrace();
			}
		}
		
		if (null != messageFormData.getMessageBody()) {
			currentMessage.setMessageBody(messageFormData.getMessageBody());
		}
		
		if (null != currentMessage.getMessageFrom() && null != currentMessage.getMessageTo()
				&& null != currentMessage.getMessageBody()) {
			long currTime = System.currentTimeMillis();
			Date tmpDate = new Date(currTime);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String formattedDate = (sdf.format(tmpDate));
			
			java.util.Date preciseDate=null;
			try {
				preciseDate = sdf.parse(formattedDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			java.sql.Date sqlDate = new java.sql.Date(preciseDate.getTime());  
			
			if(null != sqlDate) {
				currentMessage.setMessageDate(sqlDate);
			}
			
			messageService.sendMessage(currentMessage);

			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: messageMenu");
			pageData.setPageLocation("fragments/incMessage :: incMessagePage");
			pageData.setMainClass("message");
			model.addAttribute("loggedInUserId", sessionService.getCurrentUserId());
			model.addAttribute("pageData", pageData);
			model.addAttribute("messageFormData", messageFormData);
			return "main.html";
		
		} else {

			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: messageMenu");
			pageData.setPageLocation("fragments/newMessage :: newMessagePage");
			pageData.setMainClass("message");
			model.addAttribute("loggedInUserId", sessionService.getCurrentUserId());
			model.addAttribute("pageData", pageData);
			model.addAttribute("messageFormData", messageFormData);
			return "main.html";

		}
		
	}
	
	
	
	
	
	
	

}
