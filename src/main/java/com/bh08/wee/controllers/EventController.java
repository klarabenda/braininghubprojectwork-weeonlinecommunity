package com.bh08.wee.controllers;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bh08.wee.emailservice.EmailService;
import com.bh08.wee.emailservice.VEventService;
import com.bh08.wee.exceptions.NoUserException;
import com.bh08.wee.loginservice.Login;
import com.bh08.wee.model.Event;
import com.bh08.wee.model.User;
import com.bh08.wee.service.EventService;
import com.bh08.wee.service.SessionService;
import com.bh08.wee.service.UserService;
import com.bh08.wee.viewmodel.data.PageData;
import com.bh08.wee.viewmodel.data.event.NewEventFormData;
import com.bh08.wee.viewmodel.dtos.event.EventListByUserDTO;
import com.bh08.wee.viewmodel.service.NewEventFormDataService;

import com.bh08.wee.viewmodel.service.EventsByUserDataService;

@Controller
public class EventController {
	@Autowired
	private EventService eventService;
	@Autowired
	private UserService userService;
	@Autowired
	private SessionService sessionService;
	
	@Autowired
	private EventsByUserDataService userCreatedEventsService;
	@Autowired
	private Login loginControllerService;
	@Autowired
	private NewEventFormDataService eventFormDataService;


	@Autowired
	EmailService emailService;
	@Autowired
	VEventService vEventService;


	@RequestMapping(value = "new_event", method = RequestMethod.GET)
	public String displayEventForm(Model model, RedirectAttributes redirectAttributes) {
		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			return loginControllerService.handleInvalidLogin("new_event", redirectAttributes);
		} else {
			model.addAttribute("loggedInUserId", userId);
			NewEventFormData eventFormData = new NewEventFormData();
			model.addAttribute("eventFormData", eventFormData);
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: eventMenu");
			pageData.setPageLocation("fragments/new_event :: new_event_form");
			pageData.setMainClass("event");
			model.addAttribute("pageData", pageData);
			return "main.html";
		}
	}

	@RequestMapping(value = "new_event", method = RequestMethod.POST)
	public String saveEventData(@ModelAttribute("eventFormData") @Valid NewEventFormData eventFormData,
			BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {

		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			return loginControllerService.handleInvalidLogin("new_event", redirectAttributes);
		} else {
			if (!bindingResult.hasErrors()) {
				Long eventId = 0L;
				User user;
				try {
					user = userService.getUserById(userId);
					Event event = eventFormDataService.convertToEvent(user, eventFormData);
					eventService.saveEvent(event);
					eventId = event.getEventId();
					try {
						emailService.sendEvent("bendaklara@gmail.com", "Wee Event Invitation sent to " + user.getUserEmail(), event.getEventTitle(), vEventService.createVEvent(event));
					} catch (MessagingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (NoUserException e) {
					e.printStackTrace();
					redirectAttributes.addFlashAttribute("errorMessage", "There was an error loading the current user from the database.");
					return "redirect:data_access_error";
				}
				return "redirect:/event_by_user?event=" + eventId.toString();
			}
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: eventMenu");
			pageData.setPageLocation("fragments/new_event :: new_event_form");
			pageData.setMainClass("event");
			model.addAttribute("pageData", pageData);
			return "main.html";
		}

	}

	@RequestMapping(value = "events_by_user", method = RequestMethod.GET)
	public String displayFutureEvents(Model model, RedirectAttributes redirectAttributes) {
		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			return loginControllerService.handleInvalidLogin("events_by_user", redirectAttributes);
		} else {
			EventListByUserDTO userCreatedEventsData = new EventListByUserDTO();
			try {
				userCreatedEventsData = userCreatedEventsService
						.getUserCreatedFutureEvents(userService.getUserById(userId));
				System.out.println(userCreatedEventsData);
			} catch (NoUserException e) {
				e.printStackTrace();
			}
			model.addAttribute("loggedInUserId", userId);
			model.addAttribute("userCreatedEventsData", userCreatedEventsData);
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: eventMenu");
			pageData.setPageLocation("fragments/events_by_user :: users_events");
			pageData.setMainClass("event");
			model.addAttribute("pageData", pageData);
			return "main.html";
		}
	}

	@RequestMapping(value = "past_events_by_user", method = RequestMethod.GET)
	public String displayPastEvents(Model model, RedirectAttributes redirectAttributes) {
		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			return loginControllerService.handleInvalidLogin("past_events_by_user", redirectAttributes);
		} else {
			EventListByUserDTO userCreatedEventsData = new EventListByUserDTO();
			try {
				userCreatedEventsData = userCreatedEventsService
						.getUserCreatedPastEvents(userService.getUserById(userId));
				System.out.println(userCreatedEventsData);
			} catch (NoUserException e) {
				e.printStackTrace();
			}
			model.addAttribute("loggedInUserId", userId);
			model.addAttribute("userCreatedEventsData", userCreatedEventsData);
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: eventMenu");
			pageData.setPageLocation("fragments/past_events_by_user :: users_past_events");
			pageData.setMainClass("event");
			model.addAttribute("pageData", pageData);
			return "main.html";
		}
	}

}
