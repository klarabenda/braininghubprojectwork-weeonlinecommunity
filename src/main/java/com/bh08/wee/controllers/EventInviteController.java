package com.bh08.wee.controllers;

import java.io.IOException;
import java.util.Optional;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bh08.wee.emailservice.EmailService;
import com.bh08.wee.emailservice.VEventService;
import com.bh08.wee.exceptions.EventInviteNotFoundException;
import com.bh08.wee.exceptions.NoUserException;
import com.bh08.wee.loginservice.Login;
import com.bh08.wee.model.Event;
import com.bh08.wee.model.EventInvite;
import com.bh08.wee.model.User;
import com.bh08.wee.service.DepartmentService;
import com.bh08.wee.service.EventInviteService;
import com.bh08.wee.service.EventService;
import com.bh08.wee.service.SessionService;
import com.bh08.wee.service.UserService;
import com.bh08.wee.viewmodel.data.PageData;
import com.bh08.wee.viewmodel.dtos.department.DepartmentListDTO;
import com.bh08.wee.viewmodel.dtos.event.EventByUserDTO;
import com.bh08.wee.viewmodel.dtos.eventinvite.EventInviteDTO;
import com.bh08.wee.viewmodel.dtos.eventinvite.EventInviteListWithEventForUserData;
import com.bh08.wee.viewmodel.dtos.eventinvite.ExtendedEventInviteDTO;
import com.bh08.wee.viewmodel.dtos.eventinvite.ExtendedEventInviteListDTO;
import com.bh08.wee.viewmodel.dtos.user.CurrentUserDTO;
import com.bh08.wee.viewmodel.dtos.user.InvitedUserDTO;
import com.bh08.wee.viewmodel.service.DepartmentDataConverter;
import com.bh08.wee.viewmodel.service.EventsByUserDataService;
import com.bh08.wee.viewmodel.service.ExtendedEventInviteDataService;
import com.bh08.wee.viewmodel.service.EventInvitesDataService;

@Controller
public class EventInviteController {
	@Autowired
	private UserService userService;
	@Autowired
	private SessionService sessionService;
	@Autowired
	private EventService eventService;
	@Autowired
	private DepartmentService departmentService;
	@Autowired
	private EventInviteService eventInviteService;
	@Autowired
	private ExtendedEventInviteDataService extendedEventInviteService;

	@Autowired
	private Login loginControllerService;

	@Autowired
	private EventsByUserDataService eventsByUserDataService;
	@Autowired
	private EventInvitesDataService invitesDataService;

	@Autowired
	EmailService emailService;
	@Autowired
	VEventService vEventService;
	
	@RequestMapping(value = "event_by_user", method = RequestMethod.GET)
	public String displayEventForInvitation(Model model, RedirectAttributes redirectAttributes,
			@RequestParam("event") int eventId) {

		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			return loginControllerService.handleInvalidLogin("events_by_user", redirectAttributes);
		}

		else {
			Optional<Event> event = eventService.getEventById(new Long(eventId));
			if (event.isPresent()) {
				EventByUserDTO userCreatedEventData = eventsByUserDataService.getEventByUserData(event.get());
				DepartmentListDTO departmentsData = DepartmentDataConverter.createDepartmentsData(departmentService.getAllDepartments());
				model.addAttribute("loggedInUser", userId);
				model.addAttribute("userCreatedEventData", userCreatedEventData);
				model.addAttribute("departmentsData", departmentsData);
				PageData pageData = new PageData();
				pageData.setMenuLocation("fragments/menus :: eventMenu");
				pageData.setPageLocation("fragments/event_invites :: event_invites");
				pageData.setMainClass("event");
				model.addAttribute("pageData", pageData);
				return "main.html";
			} else {
				redirectAttributes.addFlashAttribute("errorMessage", "There was an error loading the selected event from the database.");
				return "redirect:data_access_error";
			}
		}
	}

	@RequestMapping(value = "invites_for_user", method = RequestMethod.GET)
	public String displayInvitations(Model model, RedirectAttributes redirectAttributes) {
		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			return loginControllerService.handleInvalidLogin("invites_for_user", redirectAttributes);
		} else {
			User user;
			try {
				user = userService.getUserById(userId);
				EventInviteListWithEventForUserData invitesForUserData = invitesDataService.getFutureInvitesForUser(user);
				model.addAttribute("loggedInUserId", userId);
				model.addAttribute("invitesForUserData", invitesForUserData);
			} catch (NoUserException e) {
				e.printStackTrace();
				redirectAttributes.addFlashAttribute("errorMessage", "There was an error loading the current user from the database.");
				return "redirect:data_access_error";
			}
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: eventMenu");
			pageData.setPageLocation("fragments/event_invites_for_user :: users_invites");
			pageData.setMainClass("event");
			model.addAttribute("pageData", pageData);
			return "main.html";
		}
	}

	
	@RequestMapping(value = "getCurrentUser", method = RequestMethod.GET)
	public @ResponseBody CurrentUserDTO getCurrentUserEmail() {
		CurrentUserDTO currentUserDto = new CurrentUserDTO();
		Long userId = sessionService.getCurrentUserId();
		System.out.println(userId);
		User user;
		try {
			user = userService.getUserById(userId);
			currentUserDto.setUserEmail(user.getUserEmail());
		} catch (NoUserException e) {
			e.printStackTrace();
		}
		
		return currentUserDto;
	}
	
	@RequestMapping(value = "getInvitedUsers", method = RequestMethod.GET)
	public @ResponseBody ExtendedEventInviteListDTO getInvitedUsers(@RequestParam("event") int eventId) {
		Optional<Event> event = eventService.getEventById(new Long(eventId));
		ExtendedEventInviteListDTO eventInvitesDto = new ExtendedEventInviteListDTO();
		if (event.isPresent()) {
			eventInvitesDto = extendedEventInviteService.getEventInvitesDto(event.get());
		}
		return eventInvitesDto;
	}

	@RequestMapping(value = "declineInvite", method = RequestMethod.POST)
	public @ResponseBody ExtendedEventInviteDTO saveDeclinedInvitation(
			@RequestBody ExtendedEventInviteDTO eventInviteDto) {
		try {
			EventInvite eventInvite = eventInviteService
					.saveDeclinedEventInvite(new Long(eventInviteDto.getEventInviteId()));
			eventInviteDto = extendedEventInviteService.getEventInviteExtendedDto(eventInvite);
		} catch (EventInviteNotFoundException e) {
			e.printStackTrace();
		} finally {
			return eventInviteDto;
		}
	}

	@RequestMapping(value = "saveInvite", method = RequestMethod.POST)
	public @ResponseBody InvitedUserDTO saveInvite(@RequestBody EventInviteDTO invitation) {
		InvitedUserDTO invitedUserDto = new InvitedUserDTO();
		User user = new User();
		try {
			Optional<Event> event = eventService.getEventById(Long.valueOf(invitation.getEventId()));
			if (event.isPresent()) {
				user = userService.getUserByUserEmail(invitation.getInvitedUserEmail());
				if (!eventInviteService.userInvitedToEvent(user, event.get())) {
					EventInvite eventInvite = new EventInvite();
					eventInvite.setEvent(event.get());
					eventInvite.setInvited(user);
					eventInvite.setInviteValid(true);
					eventInviteService.saveEventInvite(eventInvite);

					try {
						emailService.sendEvent("bendaklara@gmail.com", "Wee Event Invitation sent to " + user.getUserEmail(), event.get().getEventTitle(), vEventService.createVEvent(event.get()));
					} catch (MessagingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					invitedUserDto.setUserId(user.getUserId());
					invitedUserDto.setUserEmail(user.getUserEmail());
					invitedUserDto.setUserName(user.getUserName());
				}
			}

		} catch (NoUserException e) {
			System.out.println(e);
		} finally {
			return invitedUserDto;
		}
	}

}
