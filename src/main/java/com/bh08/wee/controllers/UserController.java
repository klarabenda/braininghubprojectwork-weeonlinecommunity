package com.bh08.wee.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.bh08.wee.exceptions.EmailInUseException;
import com.bh08.wee.exceptions.InvalidLoginException;
import com.bh08.wee.exceptions.NoUserException;
import com.bh08.wee.loginservice.Login;
import com.bh08.wee.model.Department;
import com.bh08.wee.model.Gender;
import com.bh08.wee.model.User;
import com.bh08.wee.service.ArticleService;
import com.bh08.wee.service.DepartmentService;
import com.bh08.wee.service.SessionService;
import com.bh08.wee.service.UserService;
import com.bh08.wee.viewmodel.data.PageData;
import com.bh08.wee.viewmodel.data.user.ChangePasswordFormData;
import com.bh08.wee.viewmodel.data.user.LoginFormData;
import com.bh08.wee.viewmodel.data.user.ProfileFormData;
import com.bh08.wee.viewmodel.data.user.RegistrationFormData;

@Controller
public class UserController {

	private static String UPLOADED_FOLDER = ".//src//main//webapp//";
	private User profileUser = null;

	@Autowired
	private UserService userService;

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private SessionService sessionService;
	
	@Autowired
	private ArticleService articleService;

	@Autowired
	private Login loginControllerService;
	
	@InitBinder
	public void allowEmptyDateBinding(WebDataBinder binder) {
		binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}

	@RequestMapping(value = "status", method = RequestMethod.GET)
	public String status(Model model) {
		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			userId = 0L;
		}
		model.addAttribute("loggedInUserId", userId);
		return "status.html";
	}
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(Model model) {
		LoginFormData loginFormData = new LoginFormData();
		loginFormData.setUserEmail("");
		loginFormData.setUserPassword("");
		model.addAttribute("loginFormData", loginFormData);
		return "login.html";
	}

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String submitLogin(@ModelAttribute("loginFormData") @Valid LoginFormData loginFormData,
			BindingResult bindingResult, Model model) throws InvalidLoginException, NoUserException {

		try {
			String checkEmail = loginFormData.getUserEmail();
			String checkPassword = loginFormData.getUserPassword();
			User tmpUser = userService.checkLogin(checkEmail, checkPassword);
			if (!(bindingResult.hasErrors()) && !(tmpUser.equals(null))) {
				sessionService.setCurrentUserId(tmpUser.getUserId());
//				model.addAttribute("loggedInUserId", sessionService.getCurrentUserId());
//				PageData pageData = new PageData();
//				pageData.setMenuLocation("fragments/menus :: mainPageMenu");
//				pageData.setPageLocation("fragments/mainPage :: mainPage");
//				pageData.setMainClass("main");
//				model.addAttribute("pageData", pageData);
//				model.addAttribute("Articles", articleService.getCurrentArticles());
				return "redirect:/main";
			}
		} catch (InvalidLoginException e) {
			System.out.println(e);
			bindingResult.rejectValue("userEmail", "", e.getMessage());
			return "login.html";
		} catch (Exception e) {
			System.out.println(e);
			return "login.html";
		}
		System.out.println("e");
		return "login.html";
	}

	@RequestMapping(value = "registration", method = RequestMethod.GET)
	public String changeToRegistrationSite(Model model) {
		RegistrationFormData registrationFormData = new RegistrationFormData();
		registrationFormData.setUserEmail("");
		registrationFormData.setUserPassword1("");
		registrationFormData.setUserPassword2("");
		model.addAttribute("registrationFormData", registrationFormData);
		return "registration.html";
	}

	@RequestMapping(value = "registration", method = RequestMethod.POST)
	public String registrateUser(
			@ModelAttribute("registrationFormData") @Valid RegistrationFormData registrationFormData,
			BindingResult bindingResult, Model model) {

		try {
			String regEmail = registrationFormData.getUserEmail();
			String regPassword1 = registrationFormData.getUserPassword1();
			String regPassword2 = registrationFormData.getUserPassword2();
			if (!(bindingResult.hasErrors()) && (null != regEmail) && (null != regPassword1) && (null != regPassword2)
					&& regPassword1.equals(regPassword2)) {
				userService.saveNewUser(regEmail, regPassword1);
				model.addAttribute("loginFormData", new LoginFormData());
				return "login.html";
			}
		} catch (EmailInUseException | NoUserException e) {
		}
		return "registration.html";
	}
	
	public User getCurrentUser() {
		User currentUser=null;
		List<User> users = userService.getAllUsers();
		for (User u : users) {
			if (sessionService.getCurrentUserId().equals(u.getUserId()))
				currentUser = u;
		}
		return currentUser;
	}
	
	@RequestMapping(value = "profile", method = RequestMethod.GET)
	public String changeToProfileSite(Model model, RedirectAttributes redirectAttributes) {
		
		Long userId = sessionService.getCurrentUserId();
		if (null == userId) {
			return loginControllerService.handleInvalidLogin("profile", redirectAttributes);
		} else {
			ProfileFormData profileFormData = new ProfileFormData();

			profileUser=getCurrentUser();

			profileFormData.setUserEmail(profileUser.getUserEmail());

			if (profileUser.getUserName() != null) {
				profileFormData.setUserName(profileUser.getUserName());
			}

			if (profileUser.getUserBirthPlace() != null) {
				profileFormData.setUserBirthPlace(profileUser.getUserBirthPlace());
			}

			if (profileUser.getUserBirthDate() != null) {
				profileFormData.setUserBirthDate(profileUser.getUserBirthDate().toString());
			}

			if (profileUser.getUserGender() != null) {
				profileFormData.setUserGender(profileUser.getUserGender().toString());
			}

			if (profileUser.getUserCity() != null) {
				profileFormData.setUserCity(profileUser.getUserCity());
			}

			if (profileUser.getUserDepartment() != null) {
				profileFormData.setUserDepartment(profileUser.getUserDepartment().getDepartmentName().toString());
			}

			if (profileUser.getUserSchools() != null) {
				profileFormData.setUserSchools(profileUser.getUserSchools());
			}

			if (profileUser.getUserWorkplaces() != null) {
				profileFormData.setUserWorkplaces(profileUser.getUserWorkplaces());
			}
			
			if (profileUser.getUserProfilePic()  != null) {
				profileFormData.setUserProfilePic(profileUser.getUserProfilePic());
			}
			
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: profileMenu");
			pageData.setPageLocation("fragments/profile :: profilePage");
			pageData.setMainClass("profile");
			model.addAttribute("profileUser", profileUser);
			model.addAttribute("profileFormData", profileFormData);
			model.addAttribute("loggedInUserId", sessionService.getCurrentUserId());
			model.addAttribute("pageData", pageData);
			return "main.html";
			
		}
	}

	@RequestMapping(value = "updateProfile", method = RequestMethod.POST)
	public String updateProfileDatas(@ModelAttribute("profileFormData") @Valid ProfileFormData profileFormData,
			BindingResult bindingResult, Model model) {

		if (profileFormData.getUserName() != null) {
			profileUser.setUserName(profileFormData.getUserName());
		}

		if (profileFormData.getUserBirthPlace() != null) {
			profileUser.setUserBirthPlace(profileFormData.getUserBirthPlace());
		}

		if (profileFormData.getUserBirthDate() != null) {
			LocalDate ld = LocalDate.parse(profileFormData.getUserBirthDate(),
					DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			profileUser.setUserBirthDate(ld);
		}

		if (profileFormData.getUserGender() != null) {
			profileUser.setUserGender(Gender.valueOf(profileFormData.getUserGender().toUpperCase()));
		}

		if (profileFormData.getUserCity() != null) {
			profileUser.setUserCity(profileFormData.getUserCity());
		}

		if (profileFormData.getUserDepartment() != null) {
			Optional<Department> department = departmentService.getDepartmentByName(profileFormData.getUserDepartment());
			if (department.isPresent()) {
				profileUser.setUserDepartment(department.get());
			}
		}

		if (profileFormData.getUserSchools() != null) {
			profileUser.setUserSchools(profileFormData.getUserSchools());
		}

		if (profileFormData.getUserWorkplaces() != null) {
			profileUser.setUserWorkplaces(profileFormData.getUserWorkplaces());
		}

		userService.updateUser(profileUser);

		PageData pageData = new PageData();
		pageData.setMenuLocation("fragments/menus :: mainPageMenu");
		pageData.setPageLocation("fragments/mainPage :: mainPage");
		pageData.setMainClass("profile");
		model.addAttribute("pageData", pageData);
		return "main.html";
	}

	@RequestMapping(value = "passwordchange", method = RequestMethod.GET)
	public String changeToChangePasswordPage(Model model) {
		ChangePasswordFormData changePasswordFormData = new ChangePasswordFormData();
		model.addAttribute("changePasswordFormData", changePasswordFormData);
		model.addAttribute("loggedInUserId", sessionService.getCurrentUserId());
		PageData pageData = new PageData();
		pageData.setMenuLocation("fragments/menus :: changePasswordMenu");
		pageData.setPageLocation("fragments/passwordchange :: changePasswordPage");
		pageData.setMainClass("password");
		model.addAttribute("pageData", pageData);
		return "main.html";
	}

	@RequestMapping(value = "changePassword", method = RequestMethod.POST)
	public String changePassword(
			@ModelAttribute("changePasswordFormData") @Valid ChangePasswordFormData changePasswordFormData,
			BindingResult bindingResult, Model model) {
		String tryPassword = null;
		if (null != changePasswordFormData.getOldPassword()) {
			tryPassword = DigestUtils.sha1Hex(changePasswordFormData.getOldPassword());
		}

		if (profileUser.getUserPassword().equals(tryPassword) && null != changePasswordFormData.getOldPassword()
				&& null != changePasswordFormData.getNewPassword1() && null != changePasswordFormData.getNewPassword2()
				&& changePasswordFormData.getNewPassword1().equals(changePasswordFormData.getNewPassword2())) {
			String encryptedPassword = DigestUtils.sha1Hex(changePasswordFormData.getNewPassword1());
			profileUser.setUserPassword(encryptedPassword);
			userService.updateUser(profileUser);
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: mainPageMenu");
			pageData.setPageLocation("fragments/mainPage :: mainPage");
			model.addAttribute("pageData", pageData);
			return "main.html";
		} else {
			PageData pageData = new PageData();
			pageData.setMenuLocation("fragments/menus :: changePasswordMenu");
			pageData.setPageLocation("fragments/passwordchange :: changePasswordPage");
			model.addAttribute("pageData", pageData);
			return "main.html";
		}
	}
	
	
    @PostMapping("/upload") 
    public String singleFileUpload(@RequestParam("file") MultipartFile file,
                                   RedirectAttributes redirectAttributes, Model model) {
		Long userId = sessionService.getCurrentUserId();

		System.out.println();
		if(null==userId) {
			LoginFormData loginFormData =new LoginFormData();
			redirectAttributes.addFlashAttribute("loginFormData", loginFormData );
			return "redirect:login";
		}
		else {
			Optional<User> userOptional = userService.getOptionalUserById(userId);
			if(userOptional.isPresent()) {
		        if (file.isEmpty()) {
		            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
		        }
		        try {
		            // Get the file and save it somewhere
		            byte[] bytes = file.getBytes();
	                String name = file.getOriginalFilename();
	                String extension = name.substring(name.lastIndexOf("."));
	                if(".jpg".equals(extension) || ".png".equals(extension) ) {
	                	Date date = new Date();
	                	String formattedDate = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss.SSS").format(date);
		                String newFilename = "profile_" + Long.toString(userId) + "_" + formattedDate + extension;
		                System.out.println("New filename:" + newFilename);
		                
			            Path path = Paths.get(UPLOADED_FOLDER + newFilename);
			            //Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
			            Files.write(path, bytes);
			            User user = userOptional.get();
			            user.setUserProfilePic(newFilename);
			            userService.updateUser(user);
			            redirectAttributes.addFlashAttribute("message",
			                    "You successfully uploaded '" + file.getOriginalFilename() + "'");
			            
	                }
	                else {
			            redirectAttributes.addFlashAttribute("message",
			                    "Only .png or .jpg files can be uploaded. Your file was not uploaded");
	                }

		        } catch (IOException e) {
		            e.printStackTrace();
		            redirectAttributes.addFlashAttribute("message",
		                    "There was an error uploading the file");		            
		        } 
			}
			else {
	            redirectAttributes.addFlashAttribute("message", "There was an error getting the current user from the database");
				//TODO set error message
			}
	        return "redirect:profile";
		}

    }
	

}
