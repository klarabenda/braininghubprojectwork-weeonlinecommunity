package com.bh08.wee;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class WeeApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(WeeApplication.class, args);
		// ConfigurableApplicationContext context = SpringApplication.run(new Class<?>[]
		// {WeeApplication.class, WeeConfig.class}, args);
		

	}

}
