package com.bh08.wee.viewmodel.data.message;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.bh08.wee.model.User;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class MessageFormData {
	
	@NotNull(message="Please enter a sender")
	private User messageFrom;
	
	@NotNull(message="Please enter an address")
	private User messageTo;
	
	@NotNull(message="Please enter a message body")
	private String messageBody;
	
	@NotNull(message="Please enter an address")
	private String messageToEmailAddress;
	
	@NotNull(message="Please enter a name")
	private String messageToName;
	
}
