package com.bh08.wee.viewmodel.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PageData {
	private String menuLocationTemplate;
	private String menuLocationFragment;
	private String pageLocationTemplate;
	private String pageLocationFragment;
	private String mainClass="";
	
	public void setPageLocation(String pageLocation) {
		String[] parts = pageLocation.split("::");
		
		pageLocationTemplate = parts[0].trim();
		pageLocationFragment = parts[1].trim();
	}
	
	public void setMenuLocation(String menuLocation) {
		String[] parts = menuLocation.split("::");
		
		menuLocationTemplate = parts[0].trim();
		menuLocationFragment = parts[1].trim();
	}
	
	
}
