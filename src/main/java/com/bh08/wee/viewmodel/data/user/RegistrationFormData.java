package com.bh08.wee.viewmodel.data.user;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class RegistrationFormData {
	
	@NotEmpty
	@Email
	private String userEmail;
	
	@NotEmpty
	private String userPassword1;
	
	@NotEmpty
	private String userPassword2;

	
}
