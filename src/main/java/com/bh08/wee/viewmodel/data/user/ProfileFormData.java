package com.bh08.wee.viewmodel.data.user;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.bh08.wee.model.Department;
import com.bh08.wee.model.Gender;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode

public class ProfileFormData {
	
	@NotEmpty
	private String userPassword;
	
	private String userEmail;
	private String userName;
	private String userBirthPlace;
	private String userBirthDate;
	private String userGender;
	private String userCity;
	private String userDepartment;
	private String userSchools;
	private String userWorkplaces;
	private String userProfilePic;

	
}
