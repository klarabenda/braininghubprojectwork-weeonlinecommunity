package com.bh08.wee.viewmodel.dtos.event;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface EventDTOInterface {

	public Long getEventId();

	public void setEventId(Long eventId);

	public LocalDateTime getEventFrom();

	public void setEventFrom(LocalDateTime eventFrom);

	public LocalDateTime getEventUntil();

	public void setEventUntil(LocalDateTime eventUntil);

	public LocalDate getEventFromDate();

	public void setEventFromDate(LocalDate eventFromDate);

	public String getEventFromYear();

	public void setEventFromYear(String eventFromYear);

	public String getEventFromMonth();

	public void setEventFromMonth(String eventFromMonth);

	public String getEventFromDay();

	public void setEventFromDay(String eventFromDay);

	public String getEventFromHour();

	public void setEventFromHour(String eventFromHour);

	public String getEventUntilHour();

	public void setEventUntilHour(String eventUntilHour);

	public String getEventFromMinutes();

	public void setEventFromMinutes(String eventFromMinutes);

	public String getEventUntilMinutes();

	public void setEventUntilMinutes(String eventUntilMinutes);

	public String getEventTitle();

	public void setEventTitle(String eventTitle);

	public String getEventLocation();

	public void setEventLocation(String eventLocation);

	public String getEventDescription();

	public void setEventDescription(String eventDescription);

	public Integer getInvitedCount();

	public void setInvitedCount(Integer invitedCount);

}
