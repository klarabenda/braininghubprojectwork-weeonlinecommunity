package com.bh08.wee.viewmodel.dtos.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UsersForEventInvitesDTO {
	private String eventId;
	private String name;
	private String department;

}
