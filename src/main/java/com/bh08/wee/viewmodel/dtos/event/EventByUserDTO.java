package com.bh08.wee.viewmodel.dtos.event;

import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EventByUserDTO implements EventDTOInterface {
	private Long eventId;
	private LocalDateTime eventFrom;
	private LocalDateTime eventUntil;
	private LocalDate eventFromDate;

	private String eventFromYear;
	private String eventFromMonth;
	private String eventFromDay;
	private String eventFromHour;
	private String eventUntilHour;
	private String eventFromMinutes;
	private String eventUntilMinutes;

	private String eventTitle;
	private String eventLocation;
	private String eventDescription;
	private Integer invitedCount;

}
