package com.bh08.wee.viewmodel.dtos.department;

import java.util.ArrayList;
import java.util.List;

import com.bh08.wee.model.Department;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public class DepartmentListDTO {
	List<DepartmentDTO> departmentDataList = new ArrayList<>();

	public void addDepartmentData(DepartmentDTO departmentData) {
		departmentDataList.add(departmentData);
	}

}
