package com.bh08.wee.viewmodel.dtos.eventinvite;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.bh08.wee.model.EventInvite;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ExtendedEventInviteListDTO implements Serializable {
	List<ExtendedEventInviteDTO> eventInvites;

	public ExtendedEventInviteListDTO() {
		eventInvites = new ArrayList<>();
	}

	public void addEventInvite(ExtendedEventInviteDTO eventInvite) {
		eventInvites.add(eventInvite);
	}

}
