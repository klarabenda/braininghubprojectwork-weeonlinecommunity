package com.bh08.wee.viewmodel.dtos.eventinvite;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class EventInviteDTO {

	private String eventId;
	private String invitedUserEmail;

}
