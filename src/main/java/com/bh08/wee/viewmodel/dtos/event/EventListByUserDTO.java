package com.bh08.wee.viewmodel.dtos.event;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class EventListByUserDTO {
	private List<EventByUserDTO> usersEventsList = new ArrayList<>();

	public void addUserCreatedEvent(EventByUserDTO event) {
		this.usersEventsList.add(event);
	}
}
