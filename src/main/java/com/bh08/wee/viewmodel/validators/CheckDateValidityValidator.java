package com.bh08.wee.viewmodel.validators;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.Calendar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.bh08.wee.viewmodel.data.event.NewEventFormData;
import com.bh08.wee.viewmodel.service.NewEventFormDataService;

public class CheckDateValidityValidator implements ConstraintValidator<CheckDateValidity, NewEventFormData> {
	@Autowired
	NewEventFormDataService eventFormDataService;

	@Override
	public void initialize(CheckDateValidity data) {
		// Nothing here
	}

	@Override
	public boolean isValid(NewEventFormData eventFormData, ConstraintValidatorContext context) {
		if (eventFormDataService.hasEmptyField(eventFormData)) {
			return false;
		} else {
			try {
				LocalDateTime eventFrom = eventFormDataService.getFromDateTime(eventFormData);
			} catch (DateTimeException e) {
				return false;
			}

			return true;
		}
	}
}
