package com.bh08.wee.viewmodel.validators;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.Calendar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.bh08.wee.viewmodel.data.event.NewEventFormData;
import com.bh08.wee.viewmodel.service.NewEventFormDataService;

public class CheckTemporalPrecedenceValidator
		implements ConstraintValidator<CheckTemporalPrecedence, NewEventFormData> {
	@Autowired
	NewEventFormDataService eventFormDataService;

	@Override
	public void initialize(CheckTemporalPrecedence data) {
		// Nothing here
	}

	@Override
	public boolean isValid(NewEventFormData eventFormData, ConstraintValidatorContext context) {
		if (eventFormDataService.hasEmptyField(eventFormData)) {
			return false;
		} else {

			if (eventFormDataService.getFromTime(eventFormData)
					.isBefore(eventFormDataService.getUntilTime(eventFormData))) {
				return true;
			}
			return false;
		}
	}
}
