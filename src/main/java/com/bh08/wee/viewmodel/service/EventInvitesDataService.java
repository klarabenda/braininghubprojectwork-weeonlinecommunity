package com.bh08.wee.viewmodel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.wee.model.Event;
import com.bh08.wee.model.EventInvite;
import com.bh08.wee.model.User;
import com.bh08.wee.service.EventInviteService;
import com.bh08.wee.viewmodel.dtos.eventinvite.EventInviteListWithEventForUserData;
import com.bh08.wee.viewmodel.dtos.eventinvite.EventInviteWithEventForUserDTO;

@Service
public class EventInvitesDataService implements EventsDataService<EventInviteWithEventForUserDTO> {
	@Autowired
	private EventInviteService eventInviteService;

	public EventInviteListWithEventForUserData getFutureInvitesForUser(User user) {
		EventInviteListWithEventForUserData invitesForUserData = new EventInviteListWithEventForUserData();
		List<EventInvite> eventInvitesForUser = eventInviteService.getFutureInvitesForUser(user);
		for (EventInvite eventInvite : eventInvitesForUser) {
			invitesForUserData.addInvite(getUserCreatedEvent(eventInvite));
		}
		return invitesForUserData;
	}

	private EventInviteWithEventForUserDTO getUserCreatedEvent(EventInvite eventInvite) {
		Event event = eventInvite.getEvent();
		EventInviteWithEventForUserDTO inviteForUser = new EventInviteWithEventForUserDTO();
		inviteForUser = getEventData(event, inviteForUser);
		if (eventInvite.isInviteValid()) {
			inviteForUser.setInvitationDeclined("false");
		} else {
			inviteForUser.setInvitationDeclined("true");
		}
		inviteForUser.setInvitedCount(eventInviteService.getEventInviteCount(event));
		inviteForUser.setEventCreatorName(event.getEventCreator().getUserName());
		inviteForUser.setEventCreatorEmail(event.getEventCreator().getUserEmail());
		inviteForUser.setEventInviteId(eventInvite.getInviteId().toString());
		return inviteForUser;
	}

}
