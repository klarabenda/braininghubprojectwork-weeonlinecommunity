package com.bh08.wee.viewmodel.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bh08.wee.model.Department;
import com.bh08.wee.viewmodel.dtos.department.DepartmentDTO;
import com.bh08.wee.viewmodel.dtos.department.DepartmentListDTO;

public class DepartmentDataConverter {
	
	public static DepartmentListDTO createDepartmentsData(List<Department> departments) {
		DepartmentListDTO departmentsData = new DepartmentListDTO();
		departments.forEach(department -> addDepartmentAsData(department, departmentsData));
//		for (Department department : departments) {
//			addDepartmentAsData(department, departmentsData);
//		}
		System.out.println(departmentsData);
		return departmentsData;
	}

	public static DepartmentListDTO addDepartmentAsData(Department department, DepartmentListDTO departmentsData) {
		departmentsData.addDepartmentData(
				new DepartmentDTO(department.getDepartmentId().toString(), department.getDepartmentName()));
		return departmentsData;
	}

}
