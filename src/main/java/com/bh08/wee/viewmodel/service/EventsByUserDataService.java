package com.bh08.wee.viewmodel.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bh08.wee.model.Event;
import com.bh08.wee.model.User;
import com.bh08.wee.service.EventInviteService;
import com.bh08.wee.service.EventService;
import com.bh08.wee.viewmodel.dtos.event.EventByUserDTO;
import com.bh08.wee.viewmodel.dtos.event.EventListByUserDTO;

@Service
public class EventsByUserDataService implements EventsDataService<EventByUserDTO> {
	@Autowired
	private EventService eventService;
	@Autowired
	private EventInviteService eventInviteService;

	public EventListByUserDTO getUserCreatedFutureEvents(User user) {
		EventListByUserDTO eventsByUserData = new EventListByUserDTO();
		List<Event> userCreatedEvents = eventService.getFutureEventsCreatedByUser(user);
		for (Event event : userCreatedEvents) {
			eventsByUserData.addUserCreatedEvent(getEventByUserData(event));
		}
		return eventsByUserData;
	}

	public EventListByUserDTO getUserCreatedPastEvents(User user) {
		EventListByUserDTO eventsByUserData = new EventListByUserDTO();
		List<Event> userCreatedEvents = eventService.getPastEventsCreatedByUser(user);
		for (Event event : userCreatedEvents) {
			eventsByUserData.addUserCreatedEvent(getEventByUserData(event));
		}
		return eventsByUserData;
	}

	public EventByUserDTO getEventByUserData(Event event) {
		EventByUserDTO eventByUserData = new EventByUserDTO();
		eventByUserData = getEventData(event, eventByUserData);
		eventByUserData.setInvitedCount(eventInviteService.getEventInviteCount(event));
		return eventByUserData;
	}


}
