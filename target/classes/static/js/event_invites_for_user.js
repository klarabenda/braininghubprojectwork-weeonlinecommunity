$(document).ready(function () {
    $('.declineInvitationButton').click(function (e) {
        e.preventDefault();
        let eventInviteId = $(this).attr('id'); //$(this).data("id")
        let button=$(this);

        $.ajax({
            url: './declineInvite',
            method: 'POST',
            data: JSON.stringify({ eventInviteId: eventInviteId, inviteDeclined: 'false' }),
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            success: function (data) {
                console.log(data);
                if ('true' == data.inviteDeclined) {
                    console.log(data.inviteDeclined);
                    button.replaceWith('<div>Invitation declined </div>');

                }
                else {
                    $(this).append('There was a problem. Your invitation was not saved');
                }
            }
        });


    });

});
