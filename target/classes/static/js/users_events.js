$(document).ready(function() {
	
	$('#submitButton').click(function(e) {
		e.preventDefault();
		
		console.log('Button clicked');
		
		var username = $('#username').val();
		var department = $('#department').val();
		
		$.ajax({
			url: 'http://localhost:8087/abc/search/findByDepartment?department=' + department + '&name=' + username,
			method: 'GET',
			headers: {
				'Accept': 'application/json'
			},
			success: function(data) {
				
				console.log(data);
				
				$('#results').html('');
				
				data._embedded.abc.forEach(user => {
					
					console.log(user);
					
					var row = '<tr><td>' + user.name + '</td><td>' + user.email + '</td></tr>';
					$('#results').append(row);
					
				});
			},
			error: function(xhr) {
				console.error(xhr);
			}
		});
		
	});
	
});
